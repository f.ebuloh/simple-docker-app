# Use official Nginx image as a parent image
FROM nginx:alpine

# Set the working directory  to /usr/share/nginx/html
WORKDIR /usr/share/nginx/html

# copy the current directory contents into the container working directory
COPY . .

# expose port 80
EXPOSE 80